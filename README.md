# GitlabCI templates


- jobs : 作业模板目录
- templates : 流水线模板目录



## 当前功能

- maven/npm打包
- 单元测试
- 代码扫描（多分支、sonarqube集成）
- 构建镜像（docker、container）
- 制品上传（artifactory、harbor）
- 服务部署（linux、docker、k8s）



## 使用方法

- 标准模板： 系统设置 -> CICD -> General pipelines -> Custom CI configuration path

- 个性化： 使用include引入模板文件，进行自定义参数控制。